import Vue from 'vue'
import VueRouter from 'vue-router'
// import BuyerPage from '../views/BuyerPage.vue'
import SellerPage from '../views/SellerPage.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'SellerPage',
    component: SellerPage
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
